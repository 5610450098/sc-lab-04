package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.NestedLoop;
import viewer.Gui;

public class NestedLoopC {
	private Gui frame ;
	private ActionListener list;
	private NestedLoop loops;
	
	public static void main(String[] args){
		new NestedLoopC();
	}
	
	public NestedLoopC(){
		loops = new NestedLoop();
		frame = new Gui();
		frame.setVisible(true);
		frame.setSize(500, 150);
		frame.setLocation(200, 150);
		list = new AddComboListener();
		frame.setListenerBt(list);
	}
	
	class AddComboListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(frame.getCombo() == "Nested Loop1"){
				frame.setResults(loops.nestedLoop1(frame.getnLoopField()));
			}
			else if(frame.getCombo() == "Nested Loop2"){
				frame.setResults(loops.nestedLoop2(frame.getnLoopField()));
			}
			else if(frame.getCombo() == "Nested Loop3"){
				frame.setResults(loops.nestedLoop3(frame.getnLoopField()));
			}
			else if(frame.getCombo() == "Nested Loop4"){
				frame.setResults(loops.nestedLoop4(frame.getnLoopField()));
			}
			else if(frame.getCombo() == "Nested Loop5"){
				frame.setResults(loops.nestedLoop5(frame.getnLoopField()));
			}
			else{
				frame.setResults("Please,Choose nested loops again!!!");
				
			}
		}
		
	}
}
