package viewer;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Gui extends JFrame {

	private JTextArea resultsArea;
	private JComboBox<Object> comboBox;
	private JPanel centerPanel;
	private JPanel comboPanel;
	private JButton acceptBt;
	private JTextField nLoops;
	private JLabel nLoopsLabel;
	private JScrollPane scroll;

	public Gui() {
		// TODO Auto-generated constructor stub
		createFrame();
		createPanel();
		
	}
	public void createFrame(){
		resultsArea = new JTextArea("Choose nested loops");
		nLoopsLabel = new JLabel("Enter quantily loops");
		nLoops = new JTextField(8);
		comboBox = new JComboBox<>();
		comboBox.addItem("--------------------------");
		comboBox.addItem("Nested Loop1");
		comboBox.addItem("Nested Loop2");
		comboBox.addItem("Nested Loop3");
		comboBox.addItem("Nested Loop4");
		comboBox.addItem("Nested Loop5");
		acceptBt = new JButton("Comfirm"); 
		scroll = new JScrollPane(resultsArea);
		nLoops.setText("4");
	
	}
	public void createPanel(){
		centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(1, 2));
		centerPanel.add(scroll);
		comboPanel = new JPanel();
		comboPanel.add(nLoopsLabel);
		comboPanel.add(nLoops);
		comboPanel.add(comboBox);
		comboPanel.add(acceptBt);
		centerPanel.add(comboPanel);
		add(centerPanel,BorderLayout.CENTER);
	}
	
	public void setListenerBt(ActionListener list){
		acceptBt.addActionListener(list);

	}
	public String getCombo(){
		return (String) comboBox.getSelectedItem();
	}
	public void setResults(String str){
		resultsArea.setText(str);
	}
	public int getnLoopField(){
		String str = nLoops.getText();
		int n = Integer.parseInt(str);
		return n;
	}

}
